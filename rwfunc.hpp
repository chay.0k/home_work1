#ifndef RWFUNC_HPP
#define RWFUNC_HPP
#include <iostream>

using namespace std;

struct ServiceStruct
{
    int Id; //порядковый номер
    string StateNumber; //гос номер авто
    string Name; //ФИО заказчика
    string Data; //дата ремонта
    string Time; //время ремонта
    string Type; //тип ремонта
    int Cost; //цена
};

void ReadFile(ServiceStruct*,int&,bool&); //чтение файла
void WriteFile(ServiceStruct*,int&,bool&); //сохранение файла
void Add(ServiceStruct*,int&,bool&); //добавление
void Edit(ServiceStruct*,int&,bool&); //редактирование записи
void Del(ServiceStruct*,int&,bool&); //удаление
void NewData(ServiceStruct&, string); //преобразование новых записей

#endif // RWFUNC_HPP
