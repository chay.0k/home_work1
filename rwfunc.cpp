#include "rwfunc.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include <ctype.h>

using namespace std;

void ReadFile(ServiceStruct* Service,int &NC,bool &Open)
{
    ifstream in("Service.db");
    if(!in.is_open())
    {
        cerr << "Ошибка: Файл не открыт!\n\nВозможное решение проблемы:\n"
                "1) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }
    if(Open)
    {
        cerr << "Предупреждение: Файл уже был открыт!" << endl;
        in.close();
        return;
    }

    string str;
    NC=0;
    while(!in.eof())
    {
        getline(in, str);
        if(str.empty())continue;

        ServiceStruct New;
        NewData(New, str);
        Service[NC]=New;
        NC++;
    }
    Open=true;
    in.close();
    system("cls");
    cout << "\nФайл успешно открыт!\n"<< endl;

}

void WriteFile(ServiceStruct* Service,int &NC,bool &Open)
{
    ofstream fout("ServiceCopy.db");
    if(!Open)
    {
        cerr << "Ошибка: Файл не открыт!\n\nПроверьте следующие условия:\n"
                "1) Вы ввели пункт 1 - 'Открыть файл'\n"
                "2) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "3) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }
    for(int i=0;i<NC;i++)
    {
        fout << Service[i].Id << ";"
             << Service[i].StateNumber << ";"
             << Service[i].Name << ";"
             << Service[i].Data << ";"
             << Service[i].Time << ";"
             << Service[i].Type << ";"
             << Service[i].Cost << ";"
             << endl;
    }

    fout.close();
    system("cls");
    cout << "\nФайл успешно записан!\n" << endl;
}

void Add(ServiceStruct* Service,int &NC,bool &Open)
{
    int k=1; // для проверки
    if(!Open)
    {
        cerr << "Ошибка: Файл не открыт!\n\nВозможное решение проблемы:\n"
                "1) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }

    ServiceStruct New;

    cout << "\nДобавление элемента\n\nВведите порядковый номер записи: "; cin >> New.Id;
    while (k)
    {
        k=0;
        for (int i=0;i<NC;i++)
            if(New.Id==Service[i].Id)
             {
                cout << "Порядковый номер существует, введите айди еще раз: ";
                cin >> New.Id;
                k=1;
             }
    }
    cout << "Гос. номер: "; _flushall(); getline(cin,New.StateNumber);
    cout << "ФИО владельца: "; _flushall(); getline(cin,New.Name);
    cout << "Дата обслуживания/ремонта: "; _flushall(); getline(cin,New.Data);
    cout << "Время обслуживания/ремонта: "; _flushall(); getline(cin,New.Time);
    cout << "Вид ремонта: "; _flushall(); getline(cin,New.Type);
    cout << "Цена обслуживания/ремонта "; cin >> New.Cost;
    Service[NC]=New;
    NC++;
    cout << "\nЭлемент успешно добавлен.\n" << endl;
}

void NewData(struct ServiceStruct &New, string str)
{
    string str_1,str_2,str_3,str_4,str_5,str_6,str_7;
    int index[7], i=0, j=0;
    j = str.find(';');

    while (!str.npos || i < 7)
    {
        index[i] = j;
        i++;
        j = str.find(';', j + 1);
    }

    str_1 = str.substr(0, 2);
    str_2 = str.substr(index[0] + 1, index[1] - index[0]-1);
    str_3 = str.substr(index[1] + 1, index[2] - index[1]-1);
    str_4 = str.substr(index[2] + 1, index[3] - index[2]-1);
    str_5 = str.substr(index[3] + 1, index[4] - index[3]-1);
    str_6 = str.substr(index[4] + 1, index[5] - index[4]-1);
    str_7 = str.substr(index[5] + 1, index[6] - index[5]-1);

    New.Id = stoi(str_1);
    New.StateNumber=str_2;
    New.Name=str_3;
    New.Data=str_4;
    New.Time=str_5;
    New.Type=str_6;
    New.Cost=stoi(str_7);
}

void Del(ServiceStruct* Service,int &NC,bool &Open) //удаление
{
    if(!Open)
    {
        cerr << "Ошибка: Файл не открыт!\n\nВозможное решение проблемы:\n"
                "1) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }

    system("cls");

    int k =1; // для проверки
    int Del;

    cout << "\nУдалелние элемента\nВведите ID удаляемой строки: ";
    cin >> Del;

    while (k)
    {
        for (int i=0;i<NC;i++)
        {
            if(Service[i].Id==Del)
             {
                Del=i;
                k = 0;
                break;
             }
        }
        if (k==0) break;
        cout << "Порядковый номер не существует, введите ID еще раз: ";
        cin >> Del;
     }

     for(int i=Del;i<NC-1;i++)
     {
        Service[i]=Service[i+1];
     }
     NC--;
     cout << "\nЭлемент удален!" << endl;
}

void Edit(ServiceStruct* Service,int &NC,bool &Open)
{
    int k=1; // для проверки
    if(!Open)
    {
        cerr << "Ошибка: Файл не открыт!\n\nВозможное решение проблемы:\n"
                "1) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }
    system("cls");

    ServiceStruct New;

    cout << "\nРедактирование элемента\n\nВведите порядковый номер записи: "; cin >> New.Id;
    while (k)
    {
        for (int i=0;i<NC;i++)
        {
            if(Service[i].Id==New.Id)
             {
                New.Id=i;
                k = 0;
                break;
             }
        }
        if (k==0) break;
        cout << "Порядковый номер не существует, введите ID еще раз: ";
        cin >> New.Id;
    }
    cout << "Гос. номер: "; _flushall(); getline(cin,New.StateNumber);
    cout << "ФИО владельца: "; _flushall(); getline(cin,New.Name);
    cout << "Дата обслуживания/ремонта: "; _flushall(); getline(cin,New.Data);
    cout << "Время обслуживания/ремонта: "; _flushall(); getline(cin,New.Time);
    cout << "Вид ремонта: "; _flushall(); getline(cin,New.Type);
    cout << "Цена обслуживания/ремонта "; cin >> New.Cost;

    int i = New.Id;
    Service[i].StateNumber = New.StateNumber;
    Service[i].Name = New.Name;
    Service[i].Data = New.Data;
    Service[i].Time = New.Time;
    Service[i].Type = New.Type;
    Service[i].Cost = New.Cost;

    cout << "\nЭлемент успешно отредактирован.\n" << endl;
}
