#include "menu.hpp"
#include "rwfunc.hpp"
#include <iomanip> //для setw
#include <stdlib.h>
#include <windows.h>

using namespace std;

void Menu(ServiceStruct* Service,int &NC,bool &Open)
{
    int n=0;
    system("color A");
    while(1)
    {
        system("cls");
        cout << " 1. Открыть файл" << endl;
        cout << " 2. Сохранить файл" << endl;
        cout << " 3. Создать элемент" << endl;
        cout << " 4. Удалить элемент" << endl;
        cout << " 5. Редактировать запись" << endl;
        cout << " 6. Вывести на экран" << endl;
        cout << " 7. О программе" << endl;
        cout << " 8. Выход" << endl;
        cout << "\n\nВыберите пункт меню: ";
        cin >> n;
        switch(n)
        {
            case 1: ReadFile(Service,NC,Open); break;
            case 2: WriteFile(Service,NC,Open); break;
            case 3: Add(Service,NC,Open); break;
            case 4: Del(Service,NC,Open); break;
            case 5: Edit(Service,NC,Open); break;
            case 6: PrintScreen(Service,NC,Open); break;
            case 7: FAbout(); break;
            case 8: exit(0); break;
            default: cerr << "\nДанный пункт меню отсутствует, проверьте правильность ввода!\n " << endl; break;
        }
        system("pause");
    }
}

void PrintScreen(ServiceStruct* Service,int &NC,bool &Open)
{
    if(!Open)
    {
        cerr << "Ошибка: Файл не открыт!\n\nПроверьте следующие условия:\n"
                "1) Файл с исходными данными помещён в директорию build-lab1-Desktop_Qt_5_15_1_MinGW_64_bit-Debug\n"
                "2) Файл имеет расширение .db и назван Service.db" << endl;
        return;
    }
    system("cls");
    for(int i=0;i<NC;i++)
    {
          cout << setiosflags(ios::left);
          cout << setw(3)   << Service[i].Id;
          cout << setw(15)  << Service[i].StateNumber;
          cout << setw(25)  << Service[i].Name;
          cout << setw(10)  << Service[i].Data;
          cout << setw(8)   << Service[i].Time;
          cout << setw(40)  << Service[i].Type;
          cout << setw(10)  << Service[i].Cost << endl;
    }
    cout << "\n\n";
}

void FAbout()
{
    system("cls");
    cout << "Программу выполнил Благинин Данила Всеволодович\nИЭУИС 2-4\n\n4 вариант (Автосервис)\nДата компиляции: 16.12.2020" << endl;
}
